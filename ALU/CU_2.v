module CU_2;
  reg [6:0]S;
  CU cu1(S,AS,MTR,RW,MR,MW,B,Op);
  wire AS,MTR,RW,MR,MW,B;
  wire[1:0]Op;
  
  
  initial begin
    S=7'b0110011;
    #100 S=7'b0000011;
    #100 S=7'b0100011;
    #100 S=7'b1100011;
    #100 ;
  end
endmodule