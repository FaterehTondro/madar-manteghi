module RF_2;
  reg clk,RW;
  reg [4:0] readR1, readR2, writeR;
  reg [63:0] writedata;
  wire [63:0] readD1, readD2;
  RegFile RF(clk,RW,readR1, readR2, writeR,writedata,readD1,readD2);

  initial repeat(20)#50 clk=~clk;
  initial begin 
    clk=0;
    readR1=12;
    readR2=10;
    writeR=10;
    writedata=12;
    RW=0;
    #100 RW=1;
    #100 writeR=12;
    #60 RW=0;
  end
endmodule
