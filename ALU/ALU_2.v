module ALU_2;
  reg [63:0]A,B;
  reg[3:0]C;
  wire [63:0]R;
  wire Z;
  ALU uut(A,B,C,R,Z);
  
  initial begin
    A=85;
    B=850000;
    C=4'b0010;
    #100 C=4'b0110;
    #100 C=4'b0000;
    #100 C=4'b0001;
    #100 ;
  end
endmodule
