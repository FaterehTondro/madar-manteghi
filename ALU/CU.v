module CU(input [6:0]S,output reg AS,RW,MTR,MR,MW,B,  output reg[1:0]Op);
  always @(*)begin
    case(S) 
    7'b1100011:{AS,MTR,RW,MR,MW,B,Op}=8'b00000101;
      7'b0110011:{AS,MTR,RW,MR,MW,B,OP}=8'b00100010;
      7'b0100011:{AS,MTR,RW,MR,MW,B,Op}=8'b10001000;
      7'b0000011:{AS,MTR,RW,MR,MW,B,OP}=8'b11110000;
      default:{AS,MTR,RW,MR,MW,B,Op}=8'bxxxxxxxx;
    endcase
  end
endmodule
