module ALU_CU(input[1:0]Op,input[2:0]Funct3,input[6:0]Funct7,output reg[3:0]C;
  always@(*)begin
    case(Op)
      
      2'b01:C=4'b0010;
      2'b00:C=4'b0010;
      2'b10:begin
        if(Funct7==7'b0000000 && Funct3==3'b000)C=4'b0010;
        if(Funct7==7'b0100000 && Funct3==3'b000)C=4'b0110;
        if(Funct7==7'b0000000 && Funct3==3'b111)C=4'b0000;
        if(Funct7==7'b0000000 && Funct3==3'b110)C=4'b0001;
      end
      default:C=4'bxxxx;
    endcase
  end
endmodule
