module ALU(input [63:0]A,input [63:0]B,input[3:0]C,output reg [63:0]R,output reg Z);
  
  always@(*)
  begin
      if(R==0)
    Z=1;
    else Z=0;
    case (C)
      4'b0010:R=A+B;
      4'b0110:R=A-B;
      4'b0000:R=A & B;
      4'b0001:R=A | B;
      default:R=64'hxxxxxxxxxxxxxxxx;
    endcase

  end
endmodule
