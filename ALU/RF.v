module RF (input clk,RW,input [4:0] readR1, readR2, writeR,input [63:0] witeD,output [63:0] readD1, readD2);
  integer i=0;
  reg [63:0] rf1 [31:0];
  always @(posedge clk)
  begin
    rf1[0]=0;
		  	if (RW) 
	 				rf1[writeR] <= witeD;
  end


  initial begin
    for(i=0;i<32;i=i+1)
      rf1[i]=0;
  end



  assign readD1 = rf1[readR1];
  assign readD2 = rf1[readR2];
endmodule;
