library verilog;
use verilog.vl_types.all;
entity REG8 is
    port(
        CLK             : in     vl_logic;
        INPUT           : in     vl_logic_vector(7 downto 0);
        OUTPUT          : out    vl_logic_vector(7 downto 0)
    );
end REG8;
