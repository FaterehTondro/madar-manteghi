library verilog;
use verilog.vl_types.all;
entity FULLADD1 is
    port(
        FIRSTINPUT      : in     vl_logic;
        SECONDINPUT     : in     vl_logic;
        CIN             : in     vl_logic;
        SUM             : out    vl_logic;
        COUT            : out    vl_logic
    );
end FULLADD1;
