library verilog;
use verilog.vl_types.all;
entity ADDERFOUR is
    port(
        INPUT           : in     vl_logic_vector(63 downto 0);
        OUTPUT          : out    vl_logic_vector(63 downto 0)
    );
end ADDERFOUR;
