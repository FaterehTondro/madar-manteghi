library verilog;
use verilog.vl_types.all;
entity PC is
    port(
        CLK             : in     vl_logic;
        INPUT           : in     vl_logic_vector(63 downto 0);
        OUTPUT          : out    vl_logic_vector(63 downto 0)
    );
end PC;
