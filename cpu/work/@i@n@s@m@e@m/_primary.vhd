library verilog;
use verilog.vl_types.all;
entity INSMEM is
    port(
        ADDR            : in     vl_logic_vector(63 downto 0);
        INS             : out    vl_logic_vector(31 downto 0)
    );
end INSMEM;
