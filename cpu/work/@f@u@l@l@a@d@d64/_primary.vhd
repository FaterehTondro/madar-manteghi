library verilog;
use verilog.vl_types.all;
entity FULLADD64 is
    port(
        FIRSTINPUT      : in     vl_logic_vector(63 downto 0);
        SECONDINPUT     : in     vl_logic_vector(63 downto 0);
        CIN             : in     vl_logic;
        SUM             : out    vl_logic_vector(63 downto 0);
        COUT            : out    vl_logic
    );
end FULLADD64;
