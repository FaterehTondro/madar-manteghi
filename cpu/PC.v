module PC(input CLK,input [63:0] INPUT,output [63:0] OUTPUT);
  REG8 R0(CLK,INPUT[7:0],OUTPUT[7:0]);
  REG8 R1(CLK,INPUT[15:8],OUTPUT[15:8]);
  REG8 R2(CLK,INPUT[23:16],OUTPUT[23:16]);
  REG8 R3(CLK,INPUT[31:24],OUTPUT[31:24]);
  REG8 R4(CLK,INPUT[39:32],OUTPUT[39:32]);
  REG8 R5(CLK,INPUT[47:40],OUTPUT[47:40]);
  REG8 R6(CLK,INPUT[55:48],OUTPUT[55:48]);
  REG8 R7(CLK,INPUT[63:56],OUTPUT[63:56]);
endmodule
  



