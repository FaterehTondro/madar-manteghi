module INSMEM(input [63:0] ADDR, output reg [31:0]INS);
  
  reg [31:0] M[1023:0];
  
  initial begin
    M[0]=32'h55555555;
    M[4]=32'h60606060;
  end
  
  always @(*)INS=M[ADDR];
  
endmodule


